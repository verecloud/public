#!/bin/bash

export BLUESKY_DIR=$HOME/.bluesky
export BLUESKY_SCRIPTS_DIR=$BLUESKY_DIR/bluesky.scripts

init_snippet=$( cat << EOF
#========Bluesky Scripts========
export BLUESKY_DIR=$HOME/.bluesky
export BLUESKY_SCRIPTS_DIR=$BLUESKY_DIR/bluesky.scripts

[[ -s "${BLUESKY_SCRIPTS_DIR}/bluesky-init.sh" ]] && source "${BLUESKY_SCRIPTS_DIR}/bluesky-init.sh"
#========Bluesky Scripts========
EOF
)


#Need groovy in the path
if hash groovy 2> /dev/null; then
  echo "   ___  __         ______       ";
  echo "  / _ )/ /_ _____ / __/ /____ __";
  echo " / _  / / // / -_)\ \/  '_/ // /";
  echo "/____/_/\_,_/\__/___/_/\_\\_, / ";
  echo "                         /___/  ";
  echo Installing Dependencies....
  (mkdir $BLUESKY_DIR) 1> /dev/null
  (cd $BLUESKY_DIR; git clone -q git@bitbucket.org:verecloud/bluesky.scripts.git) 1> /dev/null
  echo Done Installing Dependencies!


  bash_profile="${HOME}/.bash_profile"
  profile="${HOME}/.profile"
  bashrc="${HOME}/.bashrc"
  zshrc="${HOME}/.zshrc"

  echo "Attempt update of bash profiles..."
  if [ ! -f "$bash_profile" -a ! -f "$profile" ]; then
  	echo "#!/bin/bash" > "$bash_profile"
  	echo "$init_snippet" >> "$bash_profile"
  	echo "Created and initialised ${bash_profile}"
  else
  	if [ -f "$bash_profile" ]; then
  		if [[ -z `grep 'bluesky-init.sh' "$bash_profile"` ]]; then
  			echo -e "\n$init_snippet" >> "$bash_profile"
  			echo "Updated existing ${bash_profile}"
  		fi
  	fi

  	if [ -f "$profile" ]; then
  		if [[ -z `grep 'bluesky-init.sh' "$profile"` ]]; then
  			echo -e "\n$init_snippet" >> "$profile"
  			echo "Updated existing ${profile}"
  		fi
  	fi
  fi

  if [ ! -f "$bashrc" ]; then
  	echo "#!/bin/bash" > "$bashrc"
  	echo "$init_snippet" >> "$bashrc"
  	echo "Created and initialised ${bashrc}"
  else
  	if [[ -z `grep 'bluesky-init.sh' "$bashrc"` ]]; then
  		echo -e "\n$init_snippet" >> "$bashrc"
  		echo "Updated existing ${bashrc}"
  	fi
  fi

  echo "Attempt update of zsh profiles..."
  if [ ! -f "$zshrc" ]; then
  	echo "$init_snippet" >> "$zshrc"
  	echo "Created and initialised ${zshrc}"
  else
  	if [[ -z `grep 'bluesky-init.sh' "$zshrc"` ]]; then
  		echo -e "\n$init_snippet" >> "$zshrc"
  		echo "Updated existing ${zshrc}"
  	fi
  fi

  echo -e "\n\n\nAll done!\n\n"

  echo "Please open a new terminal, or run the following in the existing one:"
  echo ""
  echo "   source '$BLUESKY_SCRIPTS_DIR/bluesky-init.sh'"
  echo ""
  # echo "Then issue the following command:"
  # echo ""
  # echo "    sdk help"
  # echo ""
  echo "Enjoy!!!"


else
  echo 'groovy required for bluesky scripts. Aborting. '
  echo ''
  echo 'To install groovy, execute the following command:'
  echo ''
  echo 'curl -s "https://get.sdkman.io" | bash && source "${DIR}/bin/sdkman-init.sh" | sdk install groovy'
fi
